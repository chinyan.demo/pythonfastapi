from datetime import datetime, timedelta
from jose import JWTError, jwt
from passlib.context import CryptContext
import base64
import random
from src.libraries.colorlog import ColorLog
from src.config import config


################################
#   JWT
################################
def generateJWT(data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=int(config["JWT_EXPIRE_MINUTES"]))
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, config["JWT_SECRET"], algorithm=config["JWT_ALGORITHM"])
    return encoded_jwt

def generateJWTRefreshToken(data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=int(config["JWT_REFRESH_MINUTES"]))
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, config["JWT_SECRET"], algorithm=config["JWT_ALGORITHM"])
    return encoded_jwt

def getJwtDATA(token:str):
    try:
        JwtData = jwt.decode(token, config["JWT_SECRET"], algorithms=[config["JWT_ALGORITHM"]])
        return {"error":None, "data":JwtData}
    except Exception as error:
        ColorLog.Red({"error" : error})
        return {"error":True, "errMsg":error, "data":None}

################################
#   Password
################################

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verify_password(password:str,salt:str, hashed_password:str):
    stringBytes = f"{password}{salt}".encode("ascii")
    base64_bytes = base64.b64encode(stringBytes)
    base64_string = base64_bytes.decode("ascii")
    return pwd_context.verify(base64_string, hashed_password)


def hash(password:str,salt:str):
    stringBytes = f"{password}{salt}".encode("ascii")
    base64_bytes = base64.b64encode(stringBytes)
    base64_string = base64_bytes.decode("ascii")
    return pwd_context.hash(base64_string)

def randomSalt():
    randomNumb = f'{random.randint(0,99999)}'
    stringBytes = randomNumb.encode("ascii")
    base64_bytes = base64.b64encode(stringBytes)
    base64_string = base64_bytes.decode("ascii")
    return base64_string

def base64String(input:str):
    stringBytes = input.encode("ascii")
    base64_bytes = base64.b64encode(stringBytes)
    base64_string = base64_bytes.decode("ascii")
    return base64_string
