from src.libraries.colorlog import ColorLog

class Res():
    def Success(msg:str,data)->dict:
        return { 
            "success": True, 
            "error":False, 
            "code":200,
            "msg": msg,
            "data":data
        }

    def InternalServerError(msg:str,error)->dict:
        ColorLog.Red(msg,error)
        return { 
            "success": False, 
            "error":True, 
            "code":500,
            "msg": msg,
            "data":error
        }

    def BadRequest(msg:str,error)->dict:
        ColorLog.Magenta(msg,error)
        return { 
            "success": False, 
            "error":True, 
            "code":400,
            "msg": msg,
            "data":error
        }

    def Unauthorized(msg:str,error)->dict:
        ColorLog.Magenta(msg,error)
        return { 
            "success": False, 
            "error":True, 
            "code":401,
            "msg": msg,
            "data":error
        }