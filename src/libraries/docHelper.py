from uuid import uuid4 as uuidV4
from datetime import datetime
from src.db.collection.collection import Collection
from src.libraries.colorlog import ColorLog
import sys
from traceback import print_exception

def addBasefield(doc:dict, collection:str, profile_id:str="",modifier:dict={}):
    try:
        if(len(profile_id)==0):
            profileID =f'{Collection.PROFILES}/{uuidV4()}'
        else:
            profileID = profile_id

        key = f'{uuidV4()}'

        if(type(doc)!=dict):
            data = doc.dict()
            if data['id'] is None:
                del data['id']
            if data['key'] is None:
                del data['key']
            del data['createdBy']
            del data['createdAt']
            del data['updatedBy']
            del data['updatedAt']
        if(type(doc)==dict):
            data = doc

        data.update({
            "_id": f"{collection}/{key}",
            "_key": key,
            "createdBy": profileID,
            "createdAt": f'{datetime.utcnow().isoformat()}',
            "updatedBy": profileID,
            "updatedAt": f'{datetime.utcnow().isoformat()}'
        })
        data.update(modifier)

        return data
    except Exception as error:
        ColorLog.Red(error)
        print_exception(*sys.exc_info())