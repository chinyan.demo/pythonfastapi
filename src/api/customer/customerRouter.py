from fastapi import APIRouter #, Depends
from .customerController import createCustomer , listCustomer, updateCustomer, deleteCustomer ,fetchCustomer

customerRouter = APIRouter( prefix="/customer", tags= ["customer"])

customerRouter.add_api_route("/create",createCustomer,methods=["POST"])
customerRouter.add_api_route("/list",listCustomer,methods=["GET"])
customerRouter.add_api_route("/fetch",fetchCustomer,methods=["GET"])
customerRouter.add_api_route("/update",updateCustomer,methods=["PUT"])
customerRouter.add_api_route("/delete",deleteCustomer,methods=["DELETE"])