from pydantic import BaseModel
from src.db.models.profileModel import ProfileModel
from src.db.models.customerModel import CustomerModel

CreatePayload = CustomerModel

UpdatePayload = CustomerModel

class ListPayload(BaseModel):
    orgID:str

class DeletePayload(BaseModel):
    customerID:str


