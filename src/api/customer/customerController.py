import sys
from traceback import print_exception
from fastapi import Depends
from src.libraries.colorlog import ColorLog
from .customerValidator import CreatePayload, ListPayload, UpdatePayload, DeletePayload 
from src.repositories.customerRepo import customerRepo
from src.libraries.docHelper import addBasefield
from src.db.collection.collection import Collection
from src.middleware.jwt import JwtValidator
from src.libraries.response import Res


async def createCustomer(payload:CreatePayload, Jwt_Data : dict = Depends(JwtValidator)):
    try:
        #Check with Jwt OrgID
        profileID = Jwt_Data["profile"]["_id"]
        orgID = Jwt_Data["profile"]["orgID"]
    
        if payload.customerOf != orgID:
            return Res.Unauthorized("Unauthorized Action","you shall not register customer for others")

        #Inserting
        customer:dict = addBasefield(payload,Collection.CUSTOMERS,profileID)
        res = await customerRepo.create(customer,session=None)
        if res["error"] == True: 
            return Res.InternalServerError("Create Customer Failed",res["errMsg"])

        return Res.Success("Customer Registered Successfully",customer)
    except Exception as error:
        ColorLog.Red("createCustomer Controller Error : ",error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error",error)


async def listCustomer(orgID:str, Jwt_Data : dict = Depends(JwtValidator)):
    try:
        if orgID != Jwt_Data["profile"]["orgID"]: 
            return Res.Unauthorized("Unauthorized","Action Forbidden")

        res = await customerRepo.GetAllCustomerOfOrg(orgID)
        if res["error"] == True: raise Exception(res["errMsg"])

        return Res.Success("Customer Listed Successfully", res["data"])
    except Exception as error:
        ColorLog.Red("listCustomer Controller Error : ",error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error",error)

async def updateCustomer(payload:UpdatePayload, Jwt_Data : dict = Depends(JwtValidator)):
    try:
        profileID = Jwt_Data["profile"]["_id"]
        if payload.customerOf != Jwt_Data["profile"]["orgID"]: 
            return Res.Unauthorized("Unauthorized", "Action Forbidden")

        res = await customerRepo.update(payload.id,payload.dict(),profileID)
        if res["error"] == True: raise Exception(res["errMsg"])

        return Res.Success("Customer updated Successfully",payload)

    except Exception as error:
        ColorLog.Red("updateCustomer Controller Error : ",error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error",error)

async def deleteCustomer(id:str, Jwt_Data : dict = Depends(JwtValidator)):
    try:
        customerID = id
        res = await customerRepo.fetch(customerID)
        if res["error"] == True: raise Exception(res["errMsg"])


        if res["data"]["customerOf"] != Jwt_Data["profile"]["orgID"]: 
            return Res.Unauthorized("Unauthorized", "Action Forbidden")

        res = await customerRepo.delete(customerID)
        if res["error"] == True: raise Exception(res["errMsg"])
        return Res.Success("Customer deleted Successfully",None)
        
    except Exception as error:
        ColorLog.Red("deleteCustomer Controller Error : ",error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error",error)


async def fetchCustomer(id:str, Jwt_Data : dict = Depends(JwtValidator)):
    try:
        customerID = id
        res = await customerRepo.fetch(customerID)
        if res["error"] == True: raise Exception(res["errMsg"])


        if res["data"]["customerOf"] != Jwt_Data["profile"]["orgID"]: 
            return Res.Unauthorized("Unauthorized", "Action Forbidden")

        return Res.Success("Customer deleted Successfully",res["data"])
        
    except Exception as error:
        ColorLog.Red("deleteCustomer Controller Error : ",error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error",error)