from fastapi import APIRouter 
from .orgController import listOrg, fetchOrg

orgRouter = APIRouter( prefix="/org", tags= ["org"])

orgRouter.add_api_route("/list",listOrg,methods=["get"])
orgRouter.add_api_route("/fetch",fetchOrg,methods=["get"])