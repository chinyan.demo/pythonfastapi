from src.db.models.orgModel import OrgModel
from src.libraries.colorlog import ColorLog
from src.repositories.orgRepo import orgRepo
from src.middleware.jwt import JwtValidator
from fastapi import Depends
from src.libraries.response import Res

async def listOrg(Jwt_Data:dict = Depends(JwtValidator)):
    try:
        res = await orgRepo.GetAll()
        if res["error"] == True: 
            return Res.InternalServerError("List Org Failed",res["errMsg"])

        return Res.Success("Org Listed Successfully",res["data"])
    except Exception as error:
        ColorLog.Red("Org Controller Error(List) : ", error)
        return Res.InternalServerError("List Org Failed", error)

async def fetchOrg(orgID:str,Jwt_Data:dict = Depends(JwtValidator)):
    try:
        res = await orgRepo.fetch(orgID)
        if res["error"] == True: 
            return Res.InternalServerError("fetch Org Failed",res["errMsg"])

        return Res.Success("Org Fetch Successfully",res["data"])
    except Exception as error:
        ColorLog.Red("Org Controller Error(List) : ", error)
        return Res.InternalServerError("List Org Failed", error)