import sys
from traceback import print_exception
from fastapi import FastAPI
from .public.publicRouter import publicRouter
from src.libraries.colorlog import ColorLog
from .customer.customerRouter import customerRouter
from .org.orgRouter import orgRouter


def routesBinder(app:FastAPI) :
    try:
        app.include_router(publicRouter)
        app.include_router(customerRouter)
        app.include_router(orgRouter)
      
    except Exception as error:
        ColorLog.Red("routesBinder ERROR :",error)
        print_exception(*sys.exc_info())