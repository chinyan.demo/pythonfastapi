import sys
from traceback import print_exception
from src.libraries.colorlog import ColorLog
from fastapi import HTTPException
from src.api.public.publicValidator import RegisterPayload, LoginPayload
from src.libraries.docHelper import addBasefield
from src.libraries.cryptography import randomSalt, hash, generateJWT, generateJWTRefreshToken, verify_password
from src.db.collection.collection import Collection
from src.repositories.profileRepo import profileRepo
from src.repositories.accountRepo import accountRepo
from src.db.mongodb.connection import get_session
from pymongo.mongo_client import MongoClient  
from src.libraries.response import Res

async def healthCheck() -> None :
    try:
        return Res.Success("FastAPI is up and running",None)
    except Exception as error:
        ColorLog.Red("public healthcheck controller Error : ", error)
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error", error)

async def register(payload:RegisterPayload) -> None :
    try:
        email = payload.profile.email
        password = payload.hashedPassword
        salt = randomSalt()

        profile = addBasefield(payload.profile, Collection.PROFILES)
        account = addBasefield({
            "email": email,
            "userRole": "ADMIN",
            "password": {
                "salt": salt,
                "hashedPassword": hash(password,salt)
            },
            "profileID": profile["_id"]
        },Collection.ACCOUNTS)

        client:MongoClient = get_session()
        # async with will end session automatically after use
        # with client.start_session() as session:
        #     with session.start_transaction():
                
        findAccRes = await accountRepo.findOne({"email":email})
        if findAccRes["error"] == True: raise Exception(findAccRes["errMsg"])
        if findAccRes["data"] is not None: 
            # ColorLog.Yellow("findAcc : ",findAccRes["data"])
            return Res.BadRequest("Bad Request", "Email Taken") 
            
        createAccRes = await accountRepo.create(account)
        if createAccRes["error"] == True: raise Exception(createAccRes["errMsg"])
        createProfileRes = await profileRepo.create(profile)
        if createProfileRes["error"] == True: raise Exception(createProfileRes["errMsg"])

        return Res.Success("registered successfully", profile)


    except Exception as error:
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error", error)




async def login(payload:LoginPayload):
    try:
        res = await accountRepo.findOne({"email":payload.email})
        if(res["error"]==True): raise Exception(res["errMsg"])
        if res["data"] is None: 
            return Res.BadRequest("Bad Requiest: Invalid email",None)

        acc = res["data"]
        salt:str = acc["password"]["salt"]
        hashedPassword:str = acc["password"]["hashedPassword"]
        ColorLog.Magenta({"salt":salt,"hashedPassword":hashedPassword})

        if(verify_password(payload.hashedPassword,salt,hashedPassword)==True):

            # Create JWT here
            fetchProfileRes = await profileRepo.fetch(acc["profileID"])
            if fetchProfileRes["error"] == True: 
                return Res.InternalServerError("Fetch Profile failed",res["errMsg"])
            profile = fetchProfileRes["data"]
            JwtData = {"accountID": acc["_id"], "profile": profile }
        
            accessToken = generateJWT(JwtData)
            refreshToken = generateJWTRefreshToken(JwtData)
            token = { "accessToken" : accessToken, "refreshToken" : refreshToken}

            return Res.Success("Login successfully", {"token":token,"profile":profile})
        else:
            return Res.Unauthorized("Unauthorized", "Invalid login, Unauthorized !")
    except Exception as error:
        print_exception(*sys.exc_info())
        return Res.InternalServerError("Internal Server Error", error)