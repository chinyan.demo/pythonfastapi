from fastapi import APIRouter #, Depends
from .publicController import healthCheck , register, login 

publicRouter = APIRouter( prefix="", tags= ["public"])

publicRouter.add_api_route("/",healthCheck,methods=["GET"])
# publicRouter.add_api_route("/home",home,methods=["GET"])
publicRouter.add_api_route("/register",register,methods=["POST"])
publicRouter.add_api_route("/login",login,methods=["POST"])