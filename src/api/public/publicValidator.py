from pydantic import BaseModel
from src.db.models.profileModel import ProfileModel

class RegisterPayload(BaseModel):
    profile: ProfileModel
    hashedPassword: str

class LoginPayload(BaseModel):
    email: str
    hashedPassword: str