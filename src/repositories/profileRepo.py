from src.db.mongodb.baseRepository import BaseRepository 
from src.db.collection.collection import Collection  

class ProfileRepository(BaseRepository):
    def __init__(self):
         super().__init__(Collection.PROFILES)

profileRepo = ProfileRepository()