from src.db.mongodb.baseRepository import BaseRepository 
from src.db.collection.collection import Collection  
from src.libraries.colorlog import ColorLog
from traceback import print_exception
from sys import exc_info
class CustomerRepository(BaseRepository):
    def __init__(self):
         super().__init__(Collection.CUSTOMERS)

    async def GetAllCustomerOfOrg(self,orgID):
        try:
            collection = self.database[self.collectionName]
            result  = collection.find({"customerOf":orgID})

            dataList:list = []
            for document in result:
                dataList.append(document)
            return {"error": None, "data": dataList, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(FIND MANY ERROR) : ",error)
            print_exception(*exc_info())
            return {"error": True, "data": None, "errMsg": error}
            

customerRepo = CustomerRepository()