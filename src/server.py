from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.api.routesBinder import routesBinder
from src.db.mongodb.connection import close_mongo_connection
from src.db.mongodb.reset import trunscateCollections,insertIfNotExist
from src.config import config
from src.libraries.colorlog import ColorLog

def CreateServer()->FastAPI:

    app = FastAPI( 
        title="Python FastAPI Framework",
        description="Python Backend Documentation",
        version="0.0.1",
        terms_of_service="http://example.com/terms/",
        contact={
            "name": "Chin Yan",
            "url": "https://www.example.com/",
            "email": "chinyan@lifelinelab.io ",
        },
        license_info={
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        })

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    ColorLog.Magenta("APP_ENV : ",config["APP_ENV"])
    if config["APP_ENV"] == "test":
        trunscateCollections()
    
    insertIfNotExist()

    # app.add_event_handler("startup", connect_to_mongo)
    app.add_event_handler("shutdown", close_mongo_connection)

    routesBinder(app)
  
    return app