from src.db.collection.collection import Collection
from src.db.models.orgModel import OrgModel

orgSeedMatrices = {
    "collection": Collection.ORGS,
    "seed": True,
    "model":OrgModel,
    "data":[
        {
            "_id":"orgs/839834df-413a-4a7a-a5fc-a1881159230f",
            "_key":"839834df-413a-4a7a-a5fc-a1881159230f",
            "orgName":"Lembaga Arkitek Malaysia",
            "registrationNumber": "TSA-2020-01-02-EL",
            "contact":{
                "countryCode":"+60",
                "contactNumber":"0123454321",
                "fullNumber":"+60123454321",
            },
            "address":{
                "street1":"Lembaga Arkitek Malaysia",
                "street2":"Jalan Sultan Salahuddin",
                "postalCode":50480 ,
                "town":"Kuala Lumpur",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            },
            "createdBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "createdAt":"2023-01-07T10:42:20.288371",
            "updatedBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "updatedAt":"2023-01-07T10:42:20.288389"
        },

         {
            "_id":"orgs/c2b6256d-0380-4c21-a764-384be6c48dfe",
            "_key":"c2b6256d-0380-4c21-a764-384be6c48dfe",
            "orgName":"Bank Negara Malaysia",
            "registrationNumber": " MA 34137 / MA 34138",
            "contact":{
                "countryCode":"+60",
                "contactNumber":"0326988044",
                "fullNumber":"+60326988044",
            },
            "address":{
                "street1":"Bangunan Bank Negara Malaysia",
                "street2":"Jalan Dato Onn",
                "postalCode":50480  ,
                "town":"Kuala Lumpur",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            },
            "createdBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "createdAt":"2023-01-07T10:42:20.288371",
            "updatedBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "updatedAt":"2023-01-07T10:42:20.288389"
        },

         {
            "_id":"orgs/dc02393f-2d4a-4010-8848-90ff0f503787",
            "_key":"dc02393f-2d4a-4010-8848-90ff0f503787",
            "orgName":"LaLaport Bukit Bintang City Centre",
            "registrationNumber": " BBCC 34543 / BBCC 32123",
            "contact":{
                "countryCode":"+60",
                "contactNumber":"0327313555",
                "fullNumber":"+60327313555",
            },
            "address":{
                "street1":"LaLaport Bukit Bintang City Centre",
                "street2":"2, Jln Hang Tuah",
                "postalCode":55100   ,
                "town":"Bukit Bintang",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            },
            "createdBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "createdAt":"2023-01-07T10:42:20.288371",
            "updatedBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "updatedAt":"2023-01-07T10:42:20.288389"
        }
    ]
}