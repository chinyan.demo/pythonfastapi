from src.db.collection.collection import Collection
from src.db.models.accountModel import AccountModel

accountSeedMatrices = {
    "collection": Collection.ACCOUNTS,
    "seed": True,
    "model":AccountModel,
    "data":[
        {
            "_id":"accounts/5cc3897e-fcd0-46b1-a3f5-ec8c2e8bcd99",
            "_key":"5cc3897e-fcd0-46b1-a3f5-ec8c2e8bcd99",
            "email":"johndoe@gmail.com",
            "userRole":"ADMIN",
            "password":{
                "salt":"NjY5MTU=",
                "hashedPassword":"$2b$12$o2x7EvWO5Qtru9AzVjRni.TbfDj0rdP8OlIcwtHqdv66e0RUfPOKG",
            },
            "profileID":"profiles/e2f2f354-f451-49d6-aa3b-36715bca779c",
            "createdBy":"profiles/c2471e89-661f-4a96-b411-0ca2ac03187c",
            "createdAt":"2023-01-07T10:42:20.524372",
            "updatedBy":"profiles/c2471e89-661f-4a96-b411-0ca2ac03187c",
            "updatedAt":"2023-01-07T10:42:20.524390",
        }
    ]
}