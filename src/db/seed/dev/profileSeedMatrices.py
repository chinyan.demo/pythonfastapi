from src.db.collection.collection import Collection
from src.db.models.profileModel import ProfileModel

profileSeedMatrices = {
    "collection": Collection.PROFILES,
    "seed": True,
    "model":ProfileModel,
    "data":[
        {
            "_id":"profiles/e2f2f354-f451-49d6-aa3b-36715bca779c",
            "_key":"e2f2f354-f451-49d6-aa3b-36715bca779c",
            "firstName":"John",
            "lastName":"Doe",
            "email":"johndoe@gmail.com",
            "contact":{
                "countryCode":"+60",
                "contactNumber":"0123456789",
                "fullNumber":"+60123456789",
            },
            "address":[{
                "street1":"Istana Negara",
                "street2":"Jalan Tuanku Abdul Halim",
                "postalCode":50480,
                "town":"Bukit Damansara",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            }],
            "orgID":"orgs/839834df-413a-4a7a-a5fc-a1881159230f",
            "createdBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "createdAt":"2023-01-07T10:42:20.288371",
            "updatedBy":"profiles/d02a72df-5807-41f3-9c5d-c6af55ba9bf3",
            "updatedAt":"2023-01-07T10:42:20.288389",
        }
    ]
}