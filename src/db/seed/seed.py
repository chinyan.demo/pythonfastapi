from src.db.seed.test.accountSeedMatrices import accountSeedMatrices
from src.db.seed.test.profileSeedMatrices import profileSeedMatrices
from src.db.seed.test.orgSeedMatrices import orgSeedMatrices
from src.db.seed.test.customerSeedMatrices import customerSeedMatrices

from src.db.seed.dev.accountSeedMatrices import accountSeedMatrices as devAccountSeedMatrices
from src.db.seed.dev.profileSeedMatrices import profileSeedMatrices as devProfileSeedMatrices
from src.db.seed.dev.orgSeedMatrices import orgSeedMatrices as devOrgSeedMatrices
from src.db.seed.dev.customerSeedMatrices import customerSeedMatrices as devCustomerSeedMatrices

testSeedMatrices = [
    accountSeedMatrices,
    profileSeedMatrices,
    orgSeedMatrices,
    customerSeedMatrices
]

devSeedMatrices = [
    devAccountSeedMatrices,
    devProfileSeedMatrices,
    devOrgSeedMatrices,
    devCustomerSeedMatrices
]