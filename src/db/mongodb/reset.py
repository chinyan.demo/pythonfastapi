import os
import sys
from traceback import print_exception
# from motor.motor_asyncio import AsyncIOMotorDatabase
from pymongo import MongoClient, database
from pymongo.client_session import ClientSession
from src.libraries.colorlog import ColorLog
from src.db.mongodb.connection import get_session
from src.db.seed.seed import testSeedMatrices, devSeedMatrices
from src.db.collection.collection import COLLECTION_LIST
from src.config import config


def trunscateCollections()->None:
    try:
        client:MongoClient = get_session()
        db = client[config["MONGODB_DBNAME"]]
        for collectionName in COLLECTION_LIST:
            collection = db[collectionName]
            collection.drop()
    except Exception as error :
        ColorLog.Red("truncate Error : ",error)
        print_exception(*sys.exc_info())

def insertIfNotExist():
    try:
        ColorLog.Blue("seed env : ",config["APP_ENV"])
        if config["APP_ENV"] == "test" :
            seedMatrices = testSeedMatrices
        else :
            seedMatrices = devSeedMatrices

        client:MongoClient = get_session()
        db = client[config["MONGODB_DBNAME"]]
        # with client.start_session() as session:
        #     with session.start_transaction():
        for seedMatrix in seedMatrices:
            if seedMatrix["seed"] == True:
                collection = db[seedMatrix["collection"]]
                dataList = seedMatrix["data"]
                model = seedMatrix["model"]
                for data in dataList:
                    parsedData = model(**data)
                    fetched = collection.find_one({"_id":data["_id"]})
                    if fetched is None:
                        collection.insert_one(data)
                        # ColorLog.Blue("Inserted",parsedData)



    except Exception as error :
        ColorLog.Red("truncate Error : ",error)
        print_exception(*sys.exc_info())


