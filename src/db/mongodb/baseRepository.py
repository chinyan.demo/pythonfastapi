import sys
from traceback import print_exception
from datetime import datetime
from pymongo import MongoClient, database
from pymongo.client_session import ClientSession
from src.libraries.colorlog import ColorLog
from src.db.mongodb.connection import get_session
from src.config import config
# import asyncio 

class BaseRepository:
    def __init__(self,collectionName:str)->None:
        self.dbClient:MongoClient = get_session()
        self.database = self.dbClient[config["MONGODB_DBNAME"]]
        self.collectionName:str = collectionName
        
    
    async def create(self,document:dict, session:ClientSession=None):
        try:
            # ColorLog.Magenta("Entered repo function create")
            collection = self.database[self.collectionName]
            # ColorLog.Magenta("Collection OK",collection)
            # ColorLog.Magenta("document OK ::: ",document)
            result = collection.insert_one(document,session=session)
            # ColorLog.Magenta("Insert OK : ",result)
            return {"error": None, "data": result, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(INSERT ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}
    
    async def update(self,id:str,document:dict,updatedBy:str):
        try:
            document["_key"] = document["key"]
            document["id"] = document["_key"]
            document["updatedAt"] = f"{datetime.utcnow().isoformat()}"
            document["updatedBy"] = updatedBy
            del document["key"]
            del document["id"]
            collection = self.database[self.collectionName]
            result = collection.update_one({'_id': id}, {"$set":document})
            return {"error": None, "data": result, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(UPDATE ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}

    async def fetch(self,id:str):
        try:
            collection = self.database[self.collectionName]
            result = collection.find_one({'_id': id})
            return {"error": None, "data": result, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(FETCH ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}

    async def findOne(self,filter:dict):
        try:
            collection = self.database[self.collectionName]
            result = collection.find_one(filter)
            return {"error": None, "data": result, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(FindOne ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}

    async def findMany(self,filter:dict,offset:int=0,limit:int=10,sortBy:str="createdAt",sortMode="ASC"):
        try:
            SORT_MODE:dict = {
                "ASC":1,
                "DESC":-1
            }
            collection = self.database[self.collectionName]
            result  = collection.find(filter).sort(sortBy,SORT_MODE[sortMode]).skip(offset).limit(limit)

            dataList:list = []
            for document in result:
                dataList.append(document)
            return {"error": None, "data": dataList, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(FIND MANY ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}

    async def delete(self,id:str):
        try:
            collection = self.database[self.collectionName]
            result = collection.delete_one({'_id': id})
            return {"error": None, "data": result, "errMsg": None}
        except Exception as error:
            ColorLog.Red("Repository Error(DELETE ERROR) : ",error)
            print_exception(*sys.exc_info())
            return {"error": True, "data": None, "errMsg": error}