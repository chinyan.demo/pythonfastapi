import asyncio
# from motor.motor_asyncio import AsyncIOMotorClient
from pymongo.mongo_client import MongoClient
from src.config import config
from src.db.mongodb.setup import setupCollection
from src.libraries.colorlog import ColorLog
from traceback import print_exception
import sys

MONGODB_USER = config["MONGODB_USER"]
MONGODB_PASSWORD = config["MONGODB_PASSWORD"]

class DataBase:
    client: MongoClient = None


db = DataBase()


def get_session() -> MongoClient:
    return db.client

# client:AsyncIOMotorClient = motor.motor_asyncio.AsyncIOMotorClient(
#     f"mongodb+srv://{MONGODB_USER}:{urllib.parse.quote(MONGODB_PASSWORD)}@pythonappdb.mn96gsk.mongodb.net/?retryWrites=true&w=majority",
#     maxPoolSize=config['MONGODB_POOL_SIZE'], 
#     io_loop=loop
# )
# databaseInstance = client.pythonappdb

async def connect_to_mongo():
    try:
        if db.client is None:
            ColorLog.Green("Connecting To mongoDB ... ")
            ColorLog.Green(f"{config['MONGODB_URL']} ... ")
            db.client = MongoClient(
                # f"mongodb+srv://{MONGODB_USER}:{MONGODB_PASSWORD}@pythonappdb.mn96gsk.mongodb.net/?retryWrites=true&w=majority",
                config["MONGODB_URL"],
                # maxPoolSize=config['MONGODB_POOL_SIZE']
            )
            db.client.admin.command('ping')
            ColorLog.Green("Connected To mongoDB ... ")
            await setupCollection(db.client)
        else:
            ColorLog.Green("mongoDB already connected ... ")
            ColorLog.Green("Database name : ",config["MONGODB_DBNAME"])
    except Exception as error:
        ColorLog.Red("Error Connecting to mongoDB",error)
        print_exception(*sys.exc_info())
    


async def close_mongo_connection():
    ColorLog.Magenta("Disconnecting mongoDB ... ")
    db.client.close()
    ColorLog.Magenta("mongoDB disconnected successfully！")

asyncio.run(connect_to_mongo())


