from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase
from src.db.collection.collection import COLLECTION_LIST
from src.libraries.colorlog import ColorLog
from src.config import config


async def setupCollection(dbClient:AsyncIOMotorClient):
    db:AsyncIOMotorDatabase = dbClient[config["MONGODB_DBNAME"]]
    ExistingCollection = db.list_collection_names()
    for coll in COLLECTION_LIST:
        if coll in ExistingCollection:
            ColorLog.Cyan(f"Collection {coll} Exists, No creation is done")
        else:
            db.create_collection(coll)
            ColorLog.Cyan(f"Collection {coll} Created Successfully")
