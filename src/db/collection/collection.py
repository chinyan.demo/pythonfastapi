
# from enum import Enum

class Collection():
    PROFILES = "profiles"
    ACCOUNTS = "accounts"
    ORGS = "organizations"
    CUSTOMERS = "customers"

COLLECTION_LIST = [
    Collection.PROFILES,
    Collection.ACCOUNTS,
    Collection.ORGS,
    Collection.CUSTOMERS
]