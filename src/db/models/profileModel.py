from src.db.models.baseModel import BaseField, ContactComponent, AddressComponent
from typing import List

class ProfileModel(BaseField):
    firstName:str
    lastName:str
    email:str
    contact: ContactComponent
    address: List[AddressComponent]
    orgID:str