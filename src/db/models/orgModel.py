from src.db.models.baseModel import BaseField, AddressComponent, ContactComponent

class OrgModel(BaseField):
    orgName: str
    registrationNumber: str
    address: AddressComponent
    contact: ContactComponent