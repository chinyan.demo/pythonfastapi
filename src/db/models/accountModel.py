from pydantic import BaseModel
from src.db.models.baseModel import PasswordComponent,BaseField

class AccountModel(BaseField):
    email: str
    userRole: str
    password: PasswordComponent
    profileID: str
    