from pydantic import BaseModel,Field
from datetime import datetime
from typing import Optional

class BaseField(BaseModel):
    id:Optional[str] = Field(default=None,alias="_id")
    key:Optional[str] = Field(alias="_key")
    createdAt:Optional[datetime]
    createdBy:Optional[str]
    updatedAt:Optional[datetime]
    updatedBy:Optional[str]

class AddressComponent(BaseModel):
    street1: str
    street2: str
    postalCode: int
    town: str
    city:str

class ContactComponent(BaseModel):
    countryCode: str
    contactNumber: str
    fullNumber:str


class PasswordComponent(BaseModel):
    salt: str
    hashedPassword: str

class TokenComponent(BaseModel):
    privateKey: str
    accessToken: str
    refreshToken: str

