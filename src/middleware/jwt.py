from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timezone
from jose import JWTError

from src.libraries.cryptography import getJwtDATA
from src.libraries.colorlog import ColorLog
from src.config import config


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

async def JwtValidator(token: str = Depends(oauth2_scheme)):  
    try:
        res = getJwtDATA(token)
        if res["error"] == True: 
            # raise HTTPException(status_code=401, detail=res["errMsg"])
            raise JWTError(res["errMsg"])

        JwtData = res["data"]
        now = datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()
        exp = JwtData["exp"]
     
        if( now > exp ):
            raise JWTError("Unauthorizd, token expired !")
        
        yield JwtData

    except JWTError as error:
        ColorLog.Red(error)
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        raise credentials_exception