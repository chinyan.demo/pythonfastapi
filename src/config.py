from src.libraries.colorlog import ColorLog
from dotenv import dotenv_values
import os

config:dict = {}
env = "dev"

try:
    ColorLog.Magenta('os.environ["APP_ENV"] : ',os.environ["APP_ENV"])
    env = os.environ["APP_ENV"]
    ColorLog.Magenta("env : ",env)
except Exception:
    ColorLog.Magenta("env : ",env)
    
if env =="dev":    
    config = dotenv_values(".env")
else:
    config = dotenv_values("test.env") 