import uvicorn
from src.config import config
from src.server import CreateServer

app = CreateServer()

if __name__ == "__main__":
    uvicorn.run(app, host=config["HOST"], port=int(config["PORT"]))