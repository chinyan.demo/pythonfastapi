from src.libraries.colorlog import ColorLog
from src.libraries.cryptography import base64String
from fastapi.testclient import TestClient


############################
#   Register
############################
def test_register(Client:TestClient):
    payload = {
        "profile":{
            "firstName": "Steve",
            "lastName": "Jobs",
            "email":"stevejobs@apple.com",
            "contact": { 
                "countryCode": "+60",
                "contactNumber": "0173456789",
                "fullNumber":"+60173456789",
            },
            "address": [{
                "street1": "Istana Negara",
                "street2": "Jalan Tuanku Abdul Halim",
                "postalCode": 50480,
                "town": "Bukit Damansara",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            }],
            "orgID":"orgs/839834df-413a-4a7a-a5fc-a1881159230f",
        },
        "hashedPassword":base64String("somePassword@123!")
    }
    response = Client.post("/register",headers = {
        "accept": "application/json",
        "Content-Type": "application/json"
    },json = payload)
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    ColorLog.Yellow(res)
    assert res["error"] == False
    assert res["code"] == 200


############################
#   Login
############################
def test_login(Client:TestClient):
    payload = {
        "email":"johndoe@gmail.com",
        "hashedPassword":base64String("somePassword@123!")
    }
    ColorLog.Cyan("payload ::::: ",payload)
    response = Client.post("/login",headers = {
        "accept": "application/json",
        "Content-Type": "application/json"
    },json = payload)
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    ColorLog.Green(res["data"])

 
