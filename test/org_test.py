from fastapi.testclient import TestClient
from src.libraries.colorlog import ColorLog

###################################
# LIST ORG
##################################
def test_list_org(Client:TestClient,AdminToken):
    offset = 0
    limit = 10
    response = Client.get(f"/org/list?offset={offset}&limit={limit}",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    })
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    if res["error"] == True:
        ColorLog.Red(res)
    else:
        ColorLog.Green(res)
    assert res["error"] == False
    assert res["code"] == 200
    assert len(res["data"]) == 3

###################################
# FETCH ORG
##################################
def test_fetch_org(Client:TestClient,AdminToken):
    orgID = "orgs/c2b6256d-0380-4c21-a764-384be6c48dfe"
    response = Client.get(f"/org/fetch?orgID={orgID}",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    })
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    if res["error"] == True:
        ColorLog.Red(res)
    else:
        ColorLog.Green(res)
    assert res["error"] == False
    assert res["code"] == 200
    assert res["data"]["_id"] == orgID