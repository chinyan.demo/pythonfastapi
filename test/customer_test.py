from src.libraries.colorlog import ColorLog
from fastapi.testclient import TestClient

###################################
# CREATE CUSTOMER
##################################
def test_create_customer(Client:TestClient,AdminToken):
    payload = {
        "firstName": "Elon",
        "lastName": "Musk",
        "email":"elonmusk@gmail.com",
        "contact": { 
            "countryCode": "+60",
            "contactNumber": "0321708000",
            "fullNumber":"+60321708000",
        },
        "address": [{
             "street1": "13th Floor, Menara Public Bank",
             "street2": "146 Jalan Ampang",
             "postalCode": 50450,
             "town": "Bukit Damansara",
             "city":"Wilayah Persekutuan Kuala Lumpur",
        }],
        "orgID":"orgs/c2b6256d-0380-4c21-a764-384be6c48dfe", 
        "customerOf": "orgs/839834df-413a-4a7a-a5fc-a1881159230f"
    }
    response = Client.post("/customer/create",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    },json = payload)
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    assert res["error"] == False
    assert res["code"] == 200

###################################
# LIST CUSTOMER
##################################
def test_list_customer(Client:TestClient,AdminToken):
    orgID = "orgs/839834df-413a-4a7a-a5fc-a1881159230f"
    offset = 0
    limit = 10
    response = Client.get(f"/customer/list?orgID={orgID}",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    })
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    if res["error"] == True:
        ColorLog.Red(res)
    else:
        ColorLog.Green(res)
    assert res["error"] == False
    assert res["code"] == 200
    assert len(res["data"]) == 10

###################################
# FETCH CUSTOMER
##################################
def test_fetch_customer(Client:TestClient,AdminToken):
    customerID="customers/52872021-add7-4870-ad77-2dc8771624b2"
    response = Client.get(f"/customer/fetch?id={customerID}",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    })
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    if res["error"] == True:
        ColorLog.Red(res)
    else:
        ColorLog.Green(res)
    assert res["error"] == False
    assert res["code"] == 200


###################################
# UPDATE CUSTOMER
##################################
def test_update_customer(Client:TestClient,AdminToken):
    payload = {
        "_id":"customers/52872021-add7-4870-ad77-2dc8771624b2",
        "_key":"52872021-add7-4870-ad77-2dc8771624b2",
        "firstName": "Fried",
            "lastName": "Sam Bankman",
            "email":"sbf@ftxmail.com",
            "contact": { 
                "countryCode": "+60",
                "contactNumber": "0322638888",
                "fullNumber":"+6022638888",
            },
            "address": [{
                "street1": "Axiata Tower",
                "street2": "Jalan Stesen Sentral 5",
                "postalCode": 50470,
                "town": "Bangsa",
                "city":"Wilayah Persekutuan Kuala Lumpur",
            }],
            "orgID":"orgs/c2b6256d-0380-4c21-a764-384be6c48dfe", 
            "customerOf": "orgs/839834df-413a-4a7a-a5fc-a1881159230f",
            "createdBy":"profiles/e2f2f354-f451-49d6-aa3b-36715bca779c",
            "createdAt":"2023-01-08T07:51:12.002999",
            "updatedBy":"profiles/e2f2f354-f451-49d6-aa3b-36715bca779c",
            "updatedAt":"2023-01-08T07:51:12.003012"
    }
    response = Client.put("/customer/update",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    },json = payload)
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    assert res["error"] == False
    assert res["code"] == 200

###################################
# DELETE CUSTOMER
##################################
def test_delete_customer(Client:TestClient,AdminToken):
    customerID="customers/52872021-add7-4870-ad77-2dc8771624b2"
    response = Client.delete(f"/customer/delete?id={customerID}",headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {AdminToken}"
    })
    if response.status_code != 200:
        ColorLog.Red(response.text)
    assert response.status_code == 200
    res = response.json()
    if res["code"] != 200:
        ColorLog.Red(res)
    if res["error"] == True:
        ColorLog.Red(res)
    else:
        ColorLog.Green(res)
    assert res["error"] == False
    assert res["code"] == 200

