from fastapi import FastAPI
from fastapi.testclient import TestClient
from pytest import fixture
from src.server import CreateServer
import os
from src.libraries.cryptography import base64String

@fixture(scope="session")
def App() -> FastAPI:
    os.environ["APP_ENV"] = "test"
    app = CreateServer()
    return app

@fixture(scope="session")
def Client(App) -> TestClient:
    return TestClient(App)

@fixture(scope="session")
def AdminToken(Client:TestClient)->str:
    payload = {
        "email":"johndoe@gmail.com",
        "hashedPassword":base64String("somePassword@123!")
    }
    response = Client.post("/login",headers = {
        "accept": "application/json",
        "Content-Type": "application/json"
    },json = payload)
    res = response.json()
    return res["data"]["token"]["accessToken"]

